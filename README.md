# [Project] Cashiera Web

A freelance project to help local business managing their business using application (web for management and mobile for smart cashier / selling product).

## References

Mobile App: [https://gitlab.com/redevartk/cashiera-mobile](https://gitlab.com/redevartk/cashiera-mobile)

## Properties

* PHP 7.4
* Framework: Codeigniter 4.1.1
* MySQL 5.7.33+
* Init Tools: Laragon
* JQuery 3.6.0

## Troubleshoot

if there's something wrong with sql / error, pls run this :
SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));

## Installation

1. Clone repo
2. Extract system.zip
3. Run "php spark serve"
4. Enjoy

## Developer Note

* There's two database, "cashiera_server", and "tenant_1694033084" for demo
* Or you can create new account if you want
* App doesn't use any composer depedencies instead use local "system" folder
* App use per user database, it will create new database per-user
